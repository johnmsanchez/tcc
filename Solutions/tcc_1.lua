function _draw()
	cls(1)
	pal(2,2+129,1)
	rectfill(0,108,128,128,3)
	rectfill(56,98,68,107,4)
	w=1
	for i=97,16,-1 do
		line(11+w,i,113-w,i,2)
		if i%6==0 then w+=3 end
		if i%34==0 then w+=4 end
	end
	rectfill(60,14,64,14,10)
	rectfill(62,12,62,16,10)
end