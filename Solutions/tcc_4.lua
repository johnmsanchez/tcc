-- Plasma Effect Using Sine Waves

function _draw()
	t=time()
	cls(1)
	for x=0,127 do
		for y=0,127 do
			c=sin(x/20)+sin(y/20)
			pset(x,y,c*2*t)
		end
	end
end