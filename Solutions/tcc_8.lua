-- Rotating Circles With Tentacles 
::_::
  t=time()
  for i=1,5 do
    a=i/5+t/4
    circfill(64+20*cos(a),
      64+20*sin(a),4,4)
  end
  for i=0,16348 do
    x=i%128y=i/128
    pset(x,y,pget(x+1,y)+1)
  end
flip()
goto _
