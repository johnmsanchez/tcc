-- Polar Coordinates

function _draw()
	cls()
	t=time()
	for y=-64,63,2 do
		for x=-64,63,2 do
			a=atan2(y,x)*16
			d=300/sqrt(x*x+y*y)
			c=a+d+t
			pset(64+x,64+y,c)
		end
	end
end