-- Moving Text With A Sine Controller

function _draw()
	cls()
	t=time()
	text="hello world"
	for i=1,#text do
		print(text[i],
			40+4*i,60+sin(((i+t)*10)/11)*5,2
		)
	end
end